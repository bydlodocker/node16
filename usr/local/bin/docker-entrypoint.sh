#!/bin/bash

# -e Exit immediately once a command returns code other than zero
# -x Print each command before actually executing it

set -ex

# If the first argument is empty...
if [ ! -n "${1}" ]; then
  # Do nothing, exit immediately.
  exit
fi;

if \
  # If there is package.json in current folder
  [ -f "./package.json" ] && \
  # and it contains a script for that command
  (awk '/\"scripts\": \{/,/\}/' ./package.json | grep -q "\"${1}\":") && \
  # and the dependencies have been installed
  [ -d "./node_modules" ]
then
  # Consider the argument an npm script name and run it
  # with all passed arguments (serve script assumed)
  npm run "$@"

# [original behavior]
# Else if the first argument ...
elif \
  # does not start with a minus
  [ "${1#-}" != "${1}" ] || \

  # or is not a system command
  [ -z "$(command -v "${1}")" ] || \

  # or is an executable file
  [[ -f "${1}" && ! -x "${1}" ]]
then
  # Consider it a javascript source and execute using node.
  set -- node "$@"

# Otherwise...
else
  # Execute command as is
  exec "$@"
fi
